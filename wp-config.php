<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tdamblog' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@LI^S>2/72+(];,}Hy`)blXhgO/uqN2r~5Sup0V:[8Br.S%:N*Yk)cWs>aQdafY]' );
define( 'SECURE_AUTH_KEY',  '|=N?<:z@j(K7(Nh=ZG(r(_4$49-$Kl/a8e=,[Ucb{Caq+^&=`3_83g7E-.l!$aH=' );
define( 'LOGGED_IN_KEY',    'nJVTS6dLCMK]r<r~* MFvRNd(+iw[Zf!E76W;AF[>9Jwq#Y,umf8j[18+</=z<_,' );
define( 'NONCE_KEY',        'BS$66h}6}9G-AC<_;/&+@kD) 9Ow{$qE7Ow]K.r:oNSGT.(6!Ds4Aa/u{Y>bF05^' );
define( 'AUTH_SALT',        'JL/{4Hhel |?/5Wb(o y)l0`pz/QN&eUz;OPGgMLq$cY(muo94gQl23ht-<iA@f-' );
define( 'SECURE_AUTH_SALT', '/K$-~TI(IwfFX`Z:Bf_?T ;w-uIq>f~x;l^/Vr<(E+3|Ey6.yuj,q|8]XU.?~P|B' );
define( 'LOGGED_IN_SALT',   ', KY(Y<~}$&3o,m5qv0z$ax:KmagOpQ4PiUz;E%zZV(;zq+j;f2`0uxW0wrM{VD>' );
define( 'NONCE_SALT',       ':oES}_uZ.W}i>J+R0)X6pU&lQp0P?<*hKUQE^55TlpdU!`6!}_}b!!GrARGc!W&T' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false );
define( 'SCRIPT_DEBUG', true );


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
